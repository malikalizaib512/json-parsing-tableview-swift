//
//  ViewController.swift
//  SwiftChallenge
//
//  Created by XintMac on 17/05/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var animals: [String] = ["Horse", "Cow", "Camel", "Sheep", "Goat"]
    var dictionary: [String: Any] = [:]
    let cellReuseIdentifier = "cell"
    var bookings: NSDictionary = [:]
    var model = [User]()
    
    
    
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var lblLoading: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblLoading.isHidden = false
        
        
        self.getMethod()
        
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.lblLoading.isHidden = true
        self.tableView.isHidden  =  false
    }
    
    //MARK:- Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:UITableViewCell = (self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell?)!
        
        cell.textLabel?.text = self.model[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "abcd")as! DetailsViewController
        
        let arr = model[indexPath.row]
        
        
        vc.currentUser = arr
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // this method handles row deletion
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            // remove the item from the data model
            animals.remove(at: indexPath.row)
            
            // delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        } else if editingStyle == .insert {
            // Not used in our example, but if you were adding a new row, this is where you would do it.
        }
    }
    
    
    //MARK:- method to get data from API
    
    func getMethod() {
        
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/todos") else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                  error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return }
            do{
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with:
                                                                        dataResponse, options: [])
                print(jsonResponse) //Response result
                guard let jsonArray = jsonResponse as? [[String: Any]] else {
                    return
                }
                print(jsonArray)
                
                for dic in jsonArray{
                    self.model.append(User(dic)) // adding now value in Model array
                }
                self.lblLoading.isHidden = true
                self.tableView.isHidden  =  false
                
                self.tableView.reloadData()
                
                
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
        
    }
    
    
}

//MARK:- Structure to parse JSON

struct User {
    var userId: Int
    var id: Int
    var title: String
    var completed: Bool
    init(_ dictionary: [String: Any]) {
        self.userId = dictionary["userId"] as? Int ?? 0
        self.id = dictionary["id"] as? Int ?? 0
        self.title = dictionary["title"] as? String ?? ""
        self.completed = dictionary["completed"] as? Bool ?? false
    }
}
