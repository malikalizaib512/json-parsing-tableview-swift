//
//  DetailsViewController.swift
//  SwiftChallenge
//
//  Created by XintSolutions on 17/05/2021.
//

import UIKit

class DetailsViewController: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblUserId: UILabel!
    
    var currentUser:User?
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.lblId.text! =  ("\(String(describing: self.currentUser!.id))")
        self.lblTitle.text =  (self.currentUser!.title)
        self.lblUserId.text! =  ("\(String(describing: self.currentUser!.userId))")
        // Do any additional setup after loading the view.
    }
    

}
